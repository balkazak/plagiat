const button = document.getElementById('post-btn');
const requestUrl = 'http://localhost:8000/get_matches/'

button.addEventListener('click', async _ => {
    console.log(document.querySelector('#inputText').value)
    try {
        const response = await fetch(requestUrl, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "url": document.querySelector('#url').value,
                "pattern": document.querySelector('#inputText').value
            })
        })
            .then(response => response.json())
                .then(res => {
                    let html = res.page
                    for (let i = 0; i <res.matches.length; i++) {
                        html = html.replace(new RegExp(res.matches[i].search_block, "g"), res.matches[i].text_to_replace);
                    }
                    let opened = window.open("");
                    opened.document.write(html)
                });
    } catch(err) {
        console.error(`Error: ${err}`);
    }
});

//это просто анимация инпута текста, расширение по вертикали
const textarea = document.querySelector("textarea");
textarea.addEventListener("keyup", (e) => {
    textarea.style.height = "63px";
    let scHeight = e.target.scrollHeight;
    textarea.style.height = `${scHeight}px`;
});
